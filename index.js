$(document).ready( () => {
	$("#homePage").fadeToggle(1500);
	$(".marquee").marquee({
		duration: 5000,
		gap: 50,
		delayBeforeStart: 0,
		direction: 'right',
		duplicated: false
	});

	$("button").click( (e) => {
		var id = e.target.id;

		console.log(id);

		$("#homePage").fadeToggle(500).dequeue().animate({right: '500px',});
		var newPage = $("<div id=" + id + " class='main'></div>").load(id + ".html");
		$("body").append(newPage);
		$("div#" + id).fadeToggle(1000);

		if (id === "leiburcore") {
			console.log("oot mida")
			$.getJSON("leiburcore.json", function(data) {
					console.log(data[0]["name"]);
					$.each( data, (key, val) => {
						$("#albumCollection").append("<div class='album' style='background-color:" + val['bgColor'] +";'><img src="+ val['coverArt'] +"><p style='color=" + val['fgColor']+";'>" + val['name'] + "</p></div>");

					});
			});
		}
	});
});
